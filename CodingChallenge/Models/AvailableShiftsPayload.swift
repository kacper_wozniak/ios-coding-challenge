//
//  AvailableShiftsPayload.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct AvailableShiftsPayload: Decodable {

    let data: [ShiftsPerDate]

}
