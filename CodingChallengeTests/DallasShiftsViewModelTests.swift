//
//  DallasShiftsViewModelTests.swift
//  CodingChallengeTests
//
//  Created by Kacper Wozniak on 19/09/2021.
//

import XCTest
import Combine
@testable import CodingChallenge

final class MockedHttpService: HttpService {

    private let requestSubject: PassthroughSubject<Data, Error> = .init()
    private let decoder = JSONDecoder.shiftkey

    func request<T: HttpEndpoint & HasResponsePayload>(endpoint: T) -> AnyPublisher<T.ResponsePayload, Error> {
        requestSubject
            .decode(type: T.ResponsePayload.self, decoder: decoder)
            .eraseToAnyPublisher()
    }

    func sendResponse(payload: Data) {
        requestSubject.send(payload)
        requestSubject.send(completion: .finished)
    }

    func sendResponse(error: Error) {
        requestSubject.send(completion: .failure(error))
    }

}

class DallasShiftsViewModelTests: XCTestCase {

    var httpService: MockedHttpService!
    var viewModel: DallasShiftsViewModel!

    override func setUpWithError() throws {
        httpService = MockedHttpService()
        viewModel = DallasShiftsViewModel(httpService: httpService)
    }

    func testFetchAvailableShiftsWithSuccessAndSelectFirstShift() throws {
        // initial state
        XCTAssertEqual(viewModel.title, "Shifts near Dallas")
        XCTAssertEqual(viewModel.isLoading, false)
        XCTAssertNil(viewModel.error)
        XCTAssertNil(viewModel.selectedShift)
        XCTAssertEqual(viewModel.shiftsPerDates.isEmpty, true)
        // start fetching shifts
        viewModel.fetchAvailableShifts()
        XCTAssertEqual(viewModel.title, "Shifts near Dallas")
        XCTAssertEqual(viewModel.isLoading, true)
        XCTAssertNil(viewModel.error)
        XCTAssertNil(viewModel.selectedShift)
        XCTAssertEqual(viewModel.shiftsPerDates.isEmpty, true)
        // finish fetching shifts with success
        let path = Bundle.init(for: DallasShiftsViewModelTests.self)
            .path(forResource: "availableShiftsPayload", ofType: "json")!
        let data = try Data(contentsOf: URL(fileURLWithPath: path))
        httpService.sendResponse(payload: data)
        XCTAssertEqual(viewModel.title, "Shifts near Dallas")
        XCTAssertEqual(viewModel.isLoading, false)
        XCTAssertNil(viewModel.error)
        XCTAssertNil(viewModel.selectedShift)
        XCTAssertEqual(viewModel.shiftsPerDates.count, 2)
        // select first shift
        let shift = viewModel.shiftsPerDates.first!.shifts.first!
        viewModel.selected(shift: shift)
        XCTAssertEqual(viewModel.title, "Shifts near Dallas")
        XCTAssertEqual(viewModel.isLoading, false)
        XCTAssertNil(viewModel.error)
        XCTAssertEqual(viewModel.selectedShift?.id, shift.id)
        XCTAssertEqual(viewModel.shiftsPerDates.count, 2)
    }

}
