//
//  AvailableShiftsEndpoint.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct AvailableShiftsEndpoint: HttpEndpoint, HasResponsePayload {
    
    typealias ResponsePayload = AvailableShiftsPayload

    enum Period: String {
        case week, fourDays = "4day"
    }

    let method: HttpMethod = .get
    let path = "v2/available_shifts"
    let headers: [HttpHeader] = [
        AcceptHeader(contentType: .json),
        ContentTypeHeader(contentType: .json)
    ]

    let queryItems: [URLQueryItem]

    init(start: Date? = nil, period: Period? = nil, address: String? = nil, radius: Int? = nil) {
        queryItems = [
            start.map { URLQueryItem(name: "start", value: DateFormatter.shortDate.string(from: $0)) },
            period.map { URLQueryItem(name: "type", value: $0.rawValue) },
            address.map { URLQueryItem(name: "address", value: $0) },
            radius.map { URLQueryItem(name: "radius", value: $0.description) }
        ].compactMap { $0 }
    }

}
