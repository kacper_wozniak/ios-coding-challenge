//
//  ShiftDetailsViewModel.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 18/09/2021.
//

import Foundation

struct ShiftDetailsViewModel {

    struct Field: Identifiable {

        let name: String
        let value: String

        var id: String {
            name
        }

    }
    
    let fields: [Field]

    init(shift: Shift) {
        fields = [
            .init(name: "Shift kind:", value: shift.shiftKind),
            .init(name: "Start date:", value: DateFormatter.mediumDateTime.string(from: shift.startTime)),
            .init(name: "Start end:", value: DateFormatter.mediumDateTime.string(from: shift.endTime)),
            .init(name: "Facility type:", value: shift.facilityType.name),
            .init(name: "Specialty:", value: shift.localizedSpecialty.specialty.name),
            .init(name: "Skill", value: shift.skill.name),
            .init(name: "Within distance:", value: shift.withinDistance.flatMap { "\($0) mi" } ?? "-"),
            .init(name: "Covid:", value: shift.covid ? "YES" : "NO"),
            .init(name: "Premium Rate:", value: shift.premiumRate ? "YES" : "NO")
        ]
    }

    init(fields: [Field]) {
        self.fields = fields
    }

}
