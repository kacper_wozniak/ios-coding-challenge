//
//  LocalizedSpecialty.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct LocalizedSpecialty: Decodable {

    let id: Int
    let specialtyId: Int
    let stateId: Int
    let name: String
    let abbreviation: String
    let specialty: Specialty

}
