//
//  Shift.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct Shift: Decodable, Identifiable {

    let shiftId: Int
    let startTime: Date
    let endTime: Date
    let premiumRate: Bool
    let covid: Bool
    let shiftKind: String
    let withinDistance: Int?
    let facilityType: Facility
    let skill: Skill
    let localizedSpecialty: LocalizedSpecialty

    var id: Int {
        shiftId
    }
    
}
