//
//  ShiftsPerDate.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct ShiftsPerDate: Decodable, Identifiable {

    let date: Date
    let shifts: [Shift]

    var id: Date {
        date
    }

}
