//
//  Specialty.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct Specialty: Decodable {

    let id: Int
    let name: String
    let color: ColorHex
    let abbreviation: String

}
