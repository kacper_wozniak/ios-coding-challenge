//
//  ShiftDetailsView.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 18/09/2021.
//

import SwiftUI

struct ShiftDetailsView: View {

    let viewModel: ShiftDetailsViewModel

    var body: some View {
        VStack {
            List(viewModel.fields) { field in
                HStack {
                    Text(field.name)
                    Spacer()
                    Text(field.value)
                }
            }
            .listStyle(InsetGroupedListStyle())
            Button("SIGN UP", action: {})
        }
        .navigationTitle("Details")
    }

}

#if DEBUG

struct ShiftDetailsView_Previews: PreviewProvider {

    static let fields: [ShiftDetailsViewModel.Field] = {
        (1...10).map { index in
            ShiftDetailsViewModel.Field(
                name: "Field \(index):",
                value: "Value \(index)"
            )
        }
    }()

    static var previews: some View {
        NavigationView {
            ShiftDetailsView(
                viewModel: ShiftDetailsViewModel(
                    fields: fields
                )
            )
        }
    }

}

#endif
