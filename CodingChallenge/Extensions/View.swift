//
//  View.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 18/09/2021.
//

import SwiftUI

extension View {

    func foregroundColor(_ color: ColorHex) -> some View {
        foregroundColor(
            Color(
                red: Double((color.value >> 16) & 0xFF) / 255,
                green: Double((color.value >> 8) & 0xFF) / 255,
                blue: Double(color.value & 0xFF) / 255,
                opacity: 1
            )
        )
    }

}
