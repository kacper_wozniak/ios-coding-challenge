//
//  Skill.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct Skill: Decodable {

    let id: Int
    let name: String
    let color: ColorHex

}
