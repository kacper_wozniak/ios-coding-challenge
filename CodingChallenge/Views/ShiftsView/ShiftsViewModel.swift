//
//  ShiftsViewModel.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 16/09/2021.
//

import Foundation

protocol ShiftsViewModel: ObservableObject {

    var title: String { get }
    var isLoading: Bool { get }
    var shiftsPerDates: [ShiftsPerDate] { get }
    var selectedShift: Shift? { get set }
    var error: String? { get set }

    func fetchAvailableShifts()
    func displayed(shift: Shift)
    func selected(shift: Shift)

}
