//
//  String.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 18/09/2021.
//

import Foundation

extension String: Identifiable {

    public var id: Self {
        self
    }

}
