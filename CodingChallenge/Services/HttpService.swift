//
//  HttpService.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation
import Combine

protocol HasResponsePayload {

    associatedtype ResponsePayload: Decodable

}

enum HttpMethod: String {

    case get

}

protocol HttpHeader {

    var key: String { get }
    var value: String { get }

}

enum ContentType: String {

    case json = "application/json"

}

struct AcceptHeader: HttpHeader {

    let key = "Accept"
    let value: String

    init(contentType: ContentType) {
        value = contentType.rawValue
    }

}

struct ContentTypeHeader: HttpHeader {

    let key = "Content-Type"
    let value: String

    init(contentType: ContentType) {
        value = contentType.rawValue
    }

}

protocol HttpEndpoint {

    var path: String { get }
    var queryItems: [URLQueryItem] { get }
    var method: HttpMethod { get }
    var headers: [HttpHeader] { get }

}

protocol HttpService: AnyObject {

    func request<T: HttpEndpoint & HasResponsePayload>(endpoint: T) -> AnyPublisher<T.ResponsePayload, Error>

}
