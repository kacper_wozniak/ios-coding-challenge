//
//  ColorHex.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation

struct ColorHex: Decodable {

    let value: Int

    init?(value: String) {
        guard value.isValidColorHex else {
            return nil
        }
        self.value = Int(value.dropFirst(), radix: 16)!
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let value = try container.decode(String.self)
        guard value.isValidColorHex else {
            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid color format")
        }
        self.value = Int(value.dropFirst(), radix: 16)!
    }
    
}

private extension String {

    var isValidColorHex: Bool {
        let pattern = "^#[a-fA-F0-9]{6}$"
        let regex = try! NSRegularExpression(pattern: pattern)
        guard regex.firstMatch(in: self, range: NSRange(location: 0, length: count)) != nil else {
            return false
        }
        return true
    }

}
