//
//  ShiftkeyAPI.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 15/09/2021.
//

import Foundation
import Combine

class ShiftkeyAPI: HttpService {

    enum ResponseError: Error {

        case invalidRequest
        case unauthorized
        case clientError
        case serverError
        case unknown

    }

    enum Environment: String {

        case development = "dev"

    }

    private let session: URLSession
    private let decoder: JSONDecoder
    private let baseUrl: URL

    init(session: URLSession, decoder: JSONDecoder, environment: ShiftkeyAPI.Environment) {
        self.session = session
        self.decoder = decoder
        let `protocol` = "https://"
        let host = "shiftkey.com/api"
        self.baseUrl = URL(string: "\(`protocol`)\(environment.rawValue).\(host)")!
    }

    func request<T: HttpEndpoint & HasResponsePayload>(endpoint: T) -> AnyPublisher<T.ResponsePayload, Error> {
        dataTaskPublisher(for: urlRequest(for: endpoint))
            .decode(type: T.ResponsePayload.self, decoder: decoder)
            .eraseToAnyPublisher()
    }

    private func dataTaskPublisher(for request: URLRequest) -> AnyPublisher<Data, Error> {
        session
            .dataTaskPublisher(for: request)
            .receive(on: RunLoop.main)
            .tryMap { result -> Data in
                guard let httpUrlResponse = result.response as? HTTPURLResponse else {
                    fatalError("URLResponse isn't HTTPURLResponse")
                }
                guard (200..<400).contains(httpUrlResponse.statusCode) else {
                    switch httpUrlResponse.statusCode {
                    case 400:
                        throw ResponseError.invalidRequest
                    case 401:
                        throw ResponseError.unauthorized
                    case 400..<500:
                        throw ResponseError.clientError
                    case 500..<600:
                        throw ResponseError.serverError
                    default:
                        throw ResponseError.unknown
                    }
                }
                return result.data
            }
            .eraseToAnyPublisher()
    }

    private func urlRequest(for endpoint: HttpEndpoint) -> URLRequest {
        var urlComponents = URLComponents(url: baseUrl.appendingPathComponent(endpoint.path),
                                          resolvingAgainstBaseURL: true)!
        urlComponents.queryItems = endpoint.queryItems
        var urlRequest = URLRequest(url: urlComponents.url!)
        urlRequest.httpMethod = endpoint.method.rawValue.uppercased()
        endpoint.headers.forEach {
            urlRequest.addValue($0.value, forHTTPHeaderField: $0.key)
        }
        return urlRequest
    }

}
