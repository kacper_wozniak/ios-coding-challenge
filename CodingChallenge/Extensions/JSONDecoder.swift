//
//  JSONDecoder.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 19/09/2021.
//

import Foundation

extension JSONDecoder {

    static let shiftkey: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .custom({ decoder in
            let container = try decoder.singleValueContainer()
            let string = try container.decode(String.self)
            let supportedDateFormatters: [DateFormatter] = [.shortDate, .iso8601]
            for dateFormatter in supportedDateFormatters {
                if let date = dateFormatter.date(from: string) {
                    return date
                }
            }
            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Invalid date format")
        })
        return decoder
    }()

}
