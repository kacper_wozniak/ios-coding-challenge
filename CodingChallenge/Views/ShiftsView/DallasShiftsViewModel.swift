//
//  DallasShiftsViewModel.swift
//  CodingChallenge
//
//  Created by Kacper Wozniak on 18/09/2021.
//

import Foundation
import Combine

final class DallasShiftsViewModel: ShiftsViewModel {

    private let httpService: HttpService
    private let address: String
    private var startDate: Date
    private var cancellableSet: Set<AnyCancellable>

    let title = "Shifts near Dallas"
    @Published var isLoading = false
    @Published var shiftsPerDates: [ShiftsPerDate] = []
    @Published var selectedShift: Shift?
    @Published var error: String?

    init(httpService: HttpService) {
        self.httpService = httpService
        self.address = "Dallas, TX"
        self.startDate = Date()
        self.cancellableSet = []
    }

    func fetchAvailableShifts() {
        isLoading = true
        httpService
            .request(endpoint: AvailableShiftsEndpoint(start: startDate, period: .week, address: address))
            .map(\.data)
            .sink(receiveCompletion: handle(completion:), receiveValue: append(shiftsPerDates:))
            .store(in: &cancellableSet)
    }

    func displayed(shift: Shift) {
        guard shiftsPerDates.last?.shifts.last?.id == shift.id, !isLoading else { return }
        fetchAvailableShifts()
    }

    func selected(shift: Shift) {
        selectedShift = shift
    }

    private func handle(completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            self.startDate = Calendar.current.date(byAdding: .day, value: 7, to: self.startDate)!
        case .failure(let error):
            self.error = error.localizedDescription
        }
        self.isLoading = false
    }

    private func append(shiftsPerDates: [ShiftsPerDate]) {
        self.shiftsPerDates += shiftsPerDates.filter({ shiftsPerDate in
            Calendar.current.compare(shiftsPerDate.date, to: Date(), toGranularity: .day) != .orderedAscending
        })
    }

}
