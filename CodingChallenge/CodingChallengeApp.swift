//
//  CodingChallengeApp.swift
//  CodingChallenge
//
//  Created by Brady Miller on 4/7/21.
//

import SwiftUI

@main
struct CodingChallengeApp: App {

    var viewModel: DallasShiftsViewModel {
        DallasShiftsViewModel(
            httpService: ShiftkeyAPI(
                session: .shared,
                decoder: .shiftkey,
                environment: .development
            )
        )
    }

    var body: some Scene {
        WindowGroup {
            ShiftsView(viewModel: viewModel)
        }
    }
    
}
