//
//  ShiftsView.swift
//  CodingChallenge
//
//  Created by Brady Miller on 4/7/21.
//

import SwiftUI

struct ShiftsView<ViewModel: ShiftsViewModel>: View {

    @StateObject var viewModel: ViewModel

    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.shiftsPerDates) { shiftsPerDate in
                    Section(header: Text(DateFormatter.dayInWeekAndMonth.string(from: shiftsPerDate.date))) {
                        ForEach(shiftsPerDate.shifts) { shift in
                            Button(action: {
                                viewModel.selected(shift: shift)
                            }, label: {
                                shiftView(for: shift)
                            })
                            .onAppear {
                                viewModel.displayed(shift: shift)
                            }

                        }
                    }
                }
                if viewModel.isLoading {
                    HStack {
                        Spacer()
                        ProgressView()
                        Spacer()
                    }
                }
            }
            .listStyle(PlainListStyle())
            .navigationBarTitle(viewModel.title)
            .onAppear {
                viewModel.fetchAvailableShifts()
            }
            .sheet(item: $viewModel.selectedShift) { shift in
                NavigationView {
                    ShiftDetailsView(viewModel: ShiftDetailsViewModel(shift: shift))
                }
            }
            .alert(item: $viewModel.error) { error in
                Alert(title: Text(error))
            }
        }
    }

    func shiftView(for shift: Shift) -> some View {
        VStack(alignment: .leading, spacing: 4) {
            HStack {
                Text(shift.shiftKind)
                Spacer()
                Text("\(DateFormatter.shortTime.string(from: shift.startTime)) - \(DateFormatter.shortTime.string(from: shift.endTime))")
            }
            Text(shift.facilityType.name)
                .foregroundColor(shift.facilityType.color)
            Text(shift.localizedSpecialty.specialty.abbreviation)
                .foregroundColor(shift.localizedSpecialty.specialty.color)
            Text(shift.skill.name)
                .foregroundColor(shift.skill.color)
            if let distance = shift.withinDistance {
                HStack {
                    Spacer()
                    Text("\(distance) mi away")
                }
            }
        }
    }

}

#if DEBUG

final class PreviewShiftsViewModel: ShiftsViewModel {

    let title = "Shifts"
    let isLoading = true
    let shiftsPerDates: [ShiftsPerDate] = [
        ShiftsPerDate(
            date: Date(),
            shifts: [
                Shift(shiftId: 0,
                      startTime: Date(),
                      endTime: Date(),
                      premiumRate: false,
                      covid: false,
                      shiftKind: "Day shift",
                      withinDistance: 10,
                      facilityType: Facility(
                        id: 0,
                        name: "Hospital",
                        color: ColorHex(value: "#FF0000")!
                      ),
                      skill: Skill(
                        id: 0,
                        name: "Healing",
                        color: ColorHex(value: "#00FF00")!
                      ),
                      localizedSpecialty: LocalizedSpecialty(
                        id: 0,
                        specialtyId: 0,
                        stateId: 0,
                        name: "Super Nurse",
                        abbreviation: "SN",
                        specialty: Specialty(
                            id: 0,
                            name: "Super Nurse",
                            color: ColorHex(value: "#0000FF")!,
                            abbreviation: "SN")
                      )
                )
            ]
        )
    ]
    var selectedShift: Shift?
    var error: String?

    func fetchAvailableShifts() {}
    func displayed(shift: Shift) {}
    func selected(shift: Shift) { selectedShift = shift }

}

struct ShiftsView_Previews: PreviewProvider {

    static var previews: some View {
        ShiftsView(viewModel: PreviewShiftsViewModel())
    }

}

#endif
